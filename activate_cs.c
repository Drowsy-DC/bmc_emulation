#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

#include <sys/ioctl.h>

#include <linux/i2c-dev.h>
#include <i2c/smbus.h>

/* This program is an illustration of how one would send NC-SI commands over
 * MCTP over SMBus to an Intel NIC, using its sideband SMBus interface.
 *
 * It requires to be linked against the i2c library in order to compile.
 *
 * This program opens the I2C (SMBus is a subset of I2C protocol) device and
 * tries to send an MCTP frame to the Intel NIC's slave address on the SMBus.
 *
 * Our work fell short as our card never ACKed any byte (actually, it was not
 * even linked to the SMBus to being with), and actually simulating the BMC to
 * activate the critical session wouold require sending more commands. It should
 * be rather straightforward though.
 */

int main (void) {
    int bus;
    /* I2C device on the platform */
    char i2c_dev[] = "/dev/i2c-2";
    int ret = 0;
    /* Default address of Intel SMBus configuration slave */
    int addr = 0x49;
    /* MCTP command code */
    int cmd = 0x0F;
    int payload_length = 8;
    /* MCTP payload.
     * This is a basic command that was sent to us by Intel support, that is
     * used to get the endpoint UUID, and serves as a check for working MCTP
     * over SMBus. */
    unsigned char payload[256];
    payload[0] = 0x21;
    payload[1] = 0x01;
    payload[2] = 0x01;
    payload[3] = 0x00;
    payload[4] = 0xCF;
    payload[5] = 0x00;
    payload[6] = 0x85;
    payload[7] = 0x03;
    /* Other payloads include NC-SI (over MCTP over SMBus) commands such as
     * 920F 1A21 0101 00CF 0200 0100 0E00 0000 0000 0000 0000 0000 0000 0000 0098
     * which "clears initial state". */

    bus = open(i2c_dev, O_RDWR);
    if (bus < 0) {
        printf("Couldn't open \"%s\"\n", i2c_dev);
        exit(1);
    }

    if (ioctl(bus, I2C_SLAVE, addr) < 0) {
        printf("[ERRNO %d] Couldn't set the I2C slave address to 0x%x\n", errno, addr);
        exit(1);
    }
    ret = i2c_smbus_write_block_data(bus, cmd, payload_length, payload);
    if (ret < 0)
        printf("[ERRNO %d] failed sending payload\n", errno);
    else
        printf("Success!");

    close(bus);

    return 0;
}
