BMC emulation for activation of Intel critical session
======================================================

This repository is linked to the publication "Drowsy-DC: Data center power management inspired by smart phones". It contains datasets used for evaluation, as well as code to extract interesting data from the Google dataset.

The source file "activate_cs.c" contains base code to send NC-SI commands overs MCTP over SMBus. The command we wished to send activates the critical session feature of an compatible Intel NIC.

This simple program can be compiled using:
```
gcc activate_cs.c -o activate_cs -li2c
```

Note that you need the [I2C Tools library](https://git.kernel.org/pub/scm/utils/i2c-tools/i2c-tools.git/).
